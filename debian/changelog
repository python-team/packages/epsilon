epsilon (0.7.1-2) UNRELEASED; urgency=medium

  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * d/tests: Use AUTOPKGTEST_TMP instead of ADTTMP
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Ondřej Nový <novy@ondrej.org>  Tue, 29 Mar 2016 21:29:39 +0200

epsilon (0.7.1-1) unstable; urgency=medium

  * New upstream release.
  * Fix an issue where the benchmark (and the test suite) fails if the
    filesystem containing the working directory cannot be found in
    /proc/self/mounts. This is the case when building in pbuilder, among
    other restricted environments.
  * Remove obsolete XS-Testsuite field.
  * Bump Standards-Version to 3.9.6 (no changes).
  * Use pypi.debian.net redirector in debian/watch.
  * Update debian/copyright and switch to machine-readable format.
  * Fix build-depends on python-zope.interface (was using the old package
    name without a dot).

 -- Tristan Seligmann <mithrandi@debian.org>  Sat, 10 Oct 2015 17:16:14 +0200

epsilon (0.7.0-2) unstable; urgency=medium

  * Enable automated tests via autopkgtest.
  * Fix clean target (dh_clean does not actually support removing directories,
    but we were pretending it does).

 -- Tristan Seligmann <mithrandi@debian.org>  Sat, 29 Mar 2014 16:38:27 +0200

epsilon (0.7.0-1) unstable; urgency=low

  * New upstream release.
  * Bump Standards-Version.
  * Check upstream PGP signature.

 -- Tristan Seligmann <mithrandi@debian.org>  Sat, 22 Mar 2014 20:55:18 +0200

epsilon (0.6.0+r2713-2) unstable; urgency=medium

  [ Vincent Bernat ]
  * Clarify long description. Closes: #624180.

  [ Tristan Seligmann ]
  * Upload to unstable.

 -- Tristan Seligmann <mithrandi@debian.org>  Fri, 29 Nov 2013 08:22:55 +0200

epsilon (0.6.0+r2713-1) experimental; urgency=low

  [ Stefano Zacchiroli ]
  * removing myself from Uploaders

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Tristan Seligmann ]
  * Package bzr snapshot for upload to experimental.
    - Replace use of log.flushErrors which was removed. (Closes: #689498)
  * Update my email address.
  * Drop dont-use-setupclass.diff as epsilon.sslverify has now been removed
    upstream.
  * Bump Standards-Version.
  * Switch build system from cdbs to dh/pybuild.
  * Switch source format to 3.0 (quilt).
  * Fix watch file.
  * Update Homepage.

 -- Tristan Seligmann <mithrandi@debian.org>  Sun, 11 Aug 2013 08:19:14 +0200

epsilon (0.6.0-3) unstable; urgency=low

  * Fix another FTBFS due to setUpClass being removed from Twisted
    Trial. Closes: #573695.

 -- Vincent Bernat <bernat@debian.org>  Sun, 14 Mar 2010 12:08:33 +0100

epsilon (0.6.0-2) unstable; urgency=low

  [ Jakub Wilk ]
  * Fine-tune the Python 2.6 patch.

  [ Vincent Bernat ]
  * Bump Standards Version To 3.8.4.

 -- Vincent Bernat <bernat@debian.org>  Mon, 01 Mar 2010 20:56:25 +0100

epsilon (0.6.0-1) unstable; urgency=low

  * New upstream release.
     + Fix FTBFS. Closes: #564373.
  * Swap patch names for use-proc-mounts.diff and fix-test_cmp.diff.

 -- Vincent Bernat <bernat@debian.org>  Sat, 09 Jan 2010 15:26:57 +0100

epsilon (0.5.12-2) unstable; urgency=low

  [ Jakub Wilk ]
  * Apply patch from Fabrice Coutadeur to make package ready for Python 2.6
    transition (closes: #546847).
  * Bump standards version to 3.8.3

  [ Sandro Tosi ]
  * debian/README.source
    - added to comply with Policy 3.8.*

 -- Debian Python Modules Team <python-modules-team@lists.alioth.debian.org>  Tue, 24 Nov 2009 22:40:47 +0100

epsilon (0.5.12-1) unstable; urgency=low

  * New upstream release.
  * Bump Standards-Version to 3.8.1. No changes required.
  * Add myself in Uploaders.

 -- Vincent Bernat <bernat@debian.org>  Wed, 18 Mar 2009 20:48:36 +0100

epsilon (0.5.11-1) unstable; urgency=low

  [ Sandro Tosi ]
  * debian/control
    - replaces python-pyopenssl with python-openssl; Closes: #494905
  * debian/epsilon-benchmark.1
    - added
  * debian/rules
    - added manpage installation
  * debian/control
    - switch Vcs-Browser field to viewsvn

  [ Tristan Seligmann ]
  * New upstream version.
  * Bump Standards-Version.
  * Use "Copyright" instead of "(C)".
  * Remove epsilon.release; this functionality depends on Combinator which
    shouldn't be packaged, and will probably be moved into Combinator upstream
    shortly.
  * Make epsilon.scripts.benchmark use /proc/mounts instead of /etc/mtab as
    this is more reliable.
  * Fix an incorrect test failure that occurs when local time is UTC.

  [ Emilio Pozuelo Monfort ]
  * debian/rules: remove the build/ dir from the initial destination and not
    from the pysupport one, since the latter breaks if pysupport changes the
    destination folder. Closes: #516197.

 -- Tristan Seligmann <mithrandi@mithrandi.net>  Fri, 20 Feb 2009 00:24:50 +0200

epsilon (0.5.9-1) unstable; urgency=low

  [ Sandro Tosi ]
  * debian/control
    - fixed Vcs-Browser field

  [ Tristan Seligmann ]
  * New upstream release.
  * Run unit tests during build.
  * Bump Standards-Version to 3.7.3.

 -- Tristan Seligmann <mithrandi@mithrandi.net>  Sun, 02 Mar 2008 23:03:23 +0200

epsilon (0.5.7-1) unstable; urgency=low

  [ Tristan Seligmann ]
  * New upstream release.

  [ Piotr Ożarowski ]
  * Homepage field added
  * Rename XS-Vcs-* fields to Vcs-* (dpkg supports them now)

 -- Tristan Seligmann <mithrandi@mithrandi.za.net>  Sat, 10 Nov 2007 01:26:59 +0200

epsilon (0.5.0-2) unstable; urgency=low

  * upload to unstable
  * debian/control
    - tightened deps on cdbs and python-support
    - added missing build-deps on nevow and pyopenssl
    - added Tristan as an Uploader
    - removed python-specific X* fields, not needed with recent python-support
  * debian/TODO.Debian
    - added a todo item about running unit tests at build time

 -- Stefano Zacchiroli <zack@debian.org>  Sun, 01 Apr 2007 19:05:41 +0200

epsilon (0.5.0-1) experimental; urgency=low

  * Initial release (Closes: #414889)

 -- Stefano Zacchiroli <zack@debian.org>  Fri, 16 Mar 2007 11:37:31 +0100
