Source: epsilon
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Tristan Seligmann <mithrandi@debian.org>,
 Vincent Bernat <bernat@debian.org>,
Build-Depends: debhelper-compat (= 9), dh-python, python-all
Build-Depends-Indep:
 python-nevow,
 python-openssl,
 python-setuptools,
 python-twisted-core (>= 12.2.0),
 python-zope.interface,
Standards-Version: 3.9.6
Vcs-Git: https://salsa.debian.org/python-team/packages/epsilon.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/epsilon
Homepage: https://launchpad.net/epsilon

Package: python-epsilon
Architecture: all
Depends: ${misc:Depends}, ${python:Depends}
Description: Python utility modules commonly used by Divmod.org project
 Epsilon is a set of Python utility modules, commonly used by all
 Divmod.org Python projects.
 .
 This is intended mainly as a support package for code used by Divmod
 projects, however it contains many generally useful modules.
 .
 Currently included:
  * a powerful date/time formatting and import/export class
    (extime.Time), for exchanging date and time information between all
    Python's various ways to interpret objects as times or time deltas
  * tools for managing concurrent asynchronous processes within Twisted
  * a metaclass which helps you define classes with explicit states
  * a formal system for application of monkey-patches
